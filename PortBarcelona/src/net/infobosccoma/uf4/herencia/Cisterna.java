/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.infobosccoma.uf4.herencia;

/**
 *
 * @author Sergi
 */
public class Cisterna extends Contenidor {
    
    public Cisterna (String numSerie, double capacitat, Mercaderia[] mercaderies, boolean estat) {
        super(numSerie, capacitat, mercaderies, estat);                   
    }

    @Override
    public double getCapacitat() {
        return super.getCapacitat()*1000;
    }

}
