/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.infobosccoma.uf4.herencia;

/**
 *
 * @author Sergi
 */
public class Vaixell {
    private final int MAX_CONTENIDORS = 1000;
    private String[] llistaSeries;
    private double sumaVolums;

    public Vaixell(String[] llistaSeries, double sumaVolums) {
        this.llistaSeries = llistaSeries;
        this.sumaVolums = sumaVolums;
    }

    public String[] getLlistaSeries() {
        return llistaSeries;
    }

    public void setLlistaSeries(String[] llistaSeries) {
        this.llistaSeries = llistaSeries;
    }

    public double getSumaVolums() {
        return sumaVolums;
    }

    public void setSumaVolums(double sumaVolums) {
        this.sumaVolums = sumaVolums;
    }
    
    
}
