/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.infobosccoma.uf4.herencia;

import java.util.UUID;

/**
 *
 * @author Sergi
 */
public class Contenidor {
    private String numSerie;
    private double capacitat;
    private Mercaderia[] mercaderies;
    private boolean estat;

    public Contenidor(String numSerie, double capacitat, Mercaderia[] mercaderies, boolean estat) {
        this.numSerie = numSerie;
        this.capacitat = capacitat;
        this.mercaderies = mercaderies;
        this.estat = estat;
    }
    
    public String getNumSerie() {
        numSerie = UUID.randomUUID().toString();
        return numSerie;
    }

    public void setNumSerie(String numSerie) {        
        this.numSerie = numSerie;
    }

    public double getCapacitat() {
        return capacitat;
    }

    public void setCapacitat(int capacitat) {
        this.capacitat = capacitat;
    }

    public Mercaderia[] getMercaderies() {
        return mercaderies;
    }

    public void setMercaderies(Mercaderia[] mercaderies) {
        this.mercaderies = mercaderies;
    }

    public boolean isEstat() {
        return estat;
    }

    public void setEstat(boolean estat) {
        this.estat = estat;
    }
        
}


