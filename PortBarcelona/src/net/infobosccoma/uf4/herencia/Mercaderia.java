/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.infobosccoma.uf4.herencia;

/**
 *
 * @author Sergi
 */
public class Mercaderia {
    private final int MAX_MECADERIES = 100;
    private String descripcio;
    private double volum;
    private Contenidor contenidor;

    public Mercaderia(String descripcio, double volum, Contenidor contenidor) {
        this.descripcio = descripcio;
        this.volum = volum;
        this.contenidor = contenidor;
    }

    public String getDescripcio() {
        return descripcio;
    }

    public void setDescripcio(String descripcio) {
        this.descripcio = descripcio;
    }

    public double getVolum() {
        return volum;
    }

    public void setVolum(double volum) {
        this.volum = volum;
    }

    public Contenidor getContenidor() {
        return contenidor;
    }

    public void setContenidor(Contenidor contenidor) {
        this.contenidor = contenidor;
    }
}

