/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.infobosccoma.uf4.herencia;

/**
 *
 * @author Sergi
 */
public class Refrigerat extends Contenidor {
    
    private int temperatura;
    
    public Refrigerat (String numSerie, int capacitat, Mercaderia[] mercaderies, boolean estat) {
        super(numSerie, capacitat, mercaderies, estat);                   
    }   

    public int getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(int temperatura) {
        this.temperatura = temperatura;
    }
    
    @Override
    public double getCapacitat() {
        return (super.getCapacitat()*0.05)/100;
    }
    
}
